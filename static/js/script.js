let blackjackGame ={
    'player' : {'scoreSpan' : '#player-result', 
            'div': '#player-box', 
            "score": 0
            },
    'dealer' : {'scoreSpan' : '#dealer-result', 
            'div': '#dealer-box', 
            "score": 0},
    'cards': ['2', '3', '4', '5', '6', '7', '8','9','10','J','Q','K','A'],
    'cardsMap': {
        '2': 2, '3': 3,'4': 4,'5': 5,'6': 6,'7': 7,'8': 8,'9': 9,'10': 10,'J': 10,'Q': 10,'K':10,
        'A':[1,11]
    },
    'wins': 0,
    'loses': 0,
    'draws': 0,
    'isStand': false,
    'turnsOver': false};

const PLAYER = blackjackGame['player'];
const DEALER = blackjackGame['dealer'];
const HIT_SOUND = new Audio('static/sounds/swish.m4a')
const WIN_SOUND = new Audio('static/sounds/cash.mp3')
const LOSS_SOUND = new Audio('static/sounds/aww.mp3')

document.querySelector('#blackjack-hit-btn').addEventListener('click', blackjackHit);
document.querySelector('#blackjack-stand-btn').addEventListener('click', dealerLogic);
document.querySelector('#blackjack-deal-btn').addEventListener('click', blackjackNewRoundPreparation);

function blackjackHit(){
    if (blackjackGame['isStand'] == false){
        let card = pickRandomCard();
        showCard(PLAYER,card);
        updateScore(card, PLAYER);
        showCardsScore(PLAYER);
    }
}

async function dealerLogic(){
    blackjackGame['isStand'] = true;
    if(blackjackGame['turnsOver'] == false){
        let card = pickRandomCard();
        showCard(DEALER,card);
        updateScore(card, DEALER);
        showCardsScore(DEALER);

        if (PLAYER['score'] <= 21 && (DEALER['score'] <= PLAYER['score'])){
            await sleep(500);
            dealerLogic();
        }
        else{
            roundResult=blackjackJudge();
            showRoundResult(roundResult);
            blackjackGame['turnsOver'] = true;
    }
    }

}

function updateScore(card, activePlayer){
    if (card == 'A'){
        if (activePlayer['score'] + blackjackGame['cardsMap'][card][1] <= 21){
            activePlayer['score'] += blackjackGame['cardsMap'][card][1];
        }
        else{
            activePlayer['score'] += blackjackGame['cardsMap'][card][0];
        }}
    else{
        activePlayer['score'] += blackjackGame['cardsMap'][card];
    }
}

function showCard(activePlayer,card){
    if (activePlayer['score']<=21){
        let cardImage = document.createElement('img');
        cardImage.src = `static/images/${card}.png`;
        document.querySelector(activePlayer['div']).appendChild(cardImage);
        HIT_SOUND.play();
    }
}

function blackjackNewRoundPreparation(){
    if (blackjackGame['turnsOver'] == true){
        let playerImages = document.querySelector('#player-box').querySelectorAll('img');
        let dealerImages = document.querySelector('#dealer-box').querySelectorAll('img');

        for (let i=0; i < playerImages.length; i++){
            playerImages[i].remove();
        }
        for (let i=0; i < dealerImages.length; i++){
            dealerImages[i].remove();
        }

        PLAYER['score'] = 0;
        DEALER['score'] = 0;

        document.querySelector(PLAYER['scoreSpan']).textContent = "0";
        document.querySelector(DEALER['scoreSpan']).textContent = "0";

        document.querySelector(PLAYER['scoreSpan']).style.color = 'white';
        document.querySelector(DEALER['scoreSpan']).style.color = 'white';

        document.querySelector('#round-result').textContent = "Let's Play";
        document.querySelector('#round-result').style.color = 'black';

        blackjackGame["isStand"] = false;
        blackjackGame["turnsOver"] = false;
}
}

function pickRandomCard(){
    let possibleCards = blackjackGame['cards'];
    let randomCard = possibleCards[Math.floor(Math.random() * possibleCards.length)];
    return randomCard;
}

function showCardsScore(activePlayer){
    if (activePlayer['score']>21){
        document.querySelector(activePlayer['scoreSpan']).textContent = 'BUST';
        document.querySelector(activePlayer['scoreSpan']).style.color = 'red';
    }
    else{
        document.querySelector(activePlayer['scoreSpan']).textContent = activePlayer['score'];
    }
}

function blackjackJudge(){
    let winner;

    if (PLAYER['score'] <= 21){
        if ((PLAYER['score'] > DEALER['score']) || DEALER['score']>21){
            winner = PLAYER;
            blackjackGame['wins']++;
        }
        else if (PLAYER['score'] == DEALER['score']){
            blackjackGame['draws']++;
        }
        else {
            winner = DEALER;
            blackjackGame['loses']++;
        }
    }
    else {
        winner = DEALER;
        blackjackGame['loses']++;
    }
    return winner
}

function showRoundResult(winner){
    let message, messageColor;

    if (winner == PLAYER){
        document.querySelector('#wins').textContent = blackjackGame['wins'];
        message = "You Win!";
        messageColor = "green";
        WIN_SOUND.play();
    }
    else if(winner == DEALER){
        document.querySelector('#loses').textContent = blackjackGame['loses'];
        message = "You Lose!";
        messageColor = "red";
        LOSS_SOUND.play();
    }
    else {
        document.querySelector('#draws').textContent = blackjackGame['draws'];
        message = "You Drew!"
        messageColor = "black";
    }

    document.querySelector('#round-result').textContent = message;
    document.querySelector('#round-result').style.color = messageColor;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }